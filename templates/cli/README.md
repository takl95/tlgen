<h3 align="center">{{project_name}}</h3>
  <p align="center">
   {{project_summary}}
  </p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>




## About The Project

A CLI command written in NodeJS.

{{project_summary}}

## Getting Started

To get the script up and running follow these steps.

### Prerequisites

This is a of things you need to use the software.
* NodeJS

### Installation

  ```sh
  npm i -g {{project_name}}
  ```


## Usage

* Global Installation:
  ```sh
  {{project_exe}}
  ```
* with NPX
  ```sh
  npx {{project_exe}}
  ```

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request / Pull Request



## License
Distributed under the MIT License. See `LICENSE.txt` for more information.



## Contact

{{author_name}} - {{author_email}} 
