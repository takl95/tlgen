#!/usr/bin/env node

const { Command } = require("commander")

;(async () => {
  const program = new Command()
  program
    // .command("new")
    .argument("<argument>", "Output directory / Project name")
    .action((argument, options) => {
      console.log({ argument, options })
    })

  program.parse()
})()
