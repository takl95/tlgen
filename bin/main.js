#!/usr/bin/env node

const { Command } = require('commander')
const { readdirSync } = require('fs')
const SimpleScaffold = require('simple-scaffold').default
const path = require('path')
const inquirer = require('inquirer')
const chalk = require('chalk')
const fs = require('fs')

;(async () => {
  const program = new Command()
  let authorFieldDescription =
    'Your Full name (will be displayed in the LICENSE and README)'
  let emailFieldDescription =
    'Your email address (will be displayed in the README)'
  let summaryFieldDescription = 'Short summary of this project (optional)'
  let executableFieldDescription =
    'The command that this CLI script will add. (optional)'
  program
    .command('new')
    .argument(
      '<template>',
      `Template to use. For available Templates see ${chalk.yellow(
        'taklgen ls'
      )}`
    )
    .argument('<name>', 'Output directory / Project name')
    .option('-a, --author <author_name>', authorFieldDescription, null)
    .option('-e, --email <author_email>', emailFieldDescription, null)
    .option(
      '-s, --summary <summary>',
      summaryFieldDescription,
      'A cool new CLI command'
    )
    .option('-b, --executable <executable>', executableFieldDescription, null)
    .action(
      async (
        template,
        name,
        { author_name, author_email, project_summary, executable }
      ) => {
        let templatePath = path.join(__dirname, '..', 'templates', template)
        if (!fs.existsSync(templatePath)) {
          console.log(`Template ${chalk.red(template)} does not exists. `)
          console.log()
          console.log(
            `For available Templates see ${chalk.yellow('taklgen ls')}`
          )
          process.exit()
        }
        if (!author_name) {
          author_name = await inquirer
            .prompt([
              {
                name: 'name',
                message: authorFieldDescription,
                validate: (input) =>
                  !input.length ? 'A name is required' : true
              }
            ])
            .then(({ name }) => name)
        }
        if (!author_email) {
          author_email = await inquirer
            .prompt([
              {
                name: 'email',
                message: emailFieldDescription,
                validate: (input) =>
                  !input.length ? 'An email is required' : true
              }
            ])
            .then(({ email }) => email)
        }
        if (!project_summary) {
          project_summary = await inquirer
            .prompt([
              {
                name: 'summary',
                message: summaryFieldDescription
              }
            ])
            .then(({ summary }) => summary)
        }
        if (!executable) {
          executable = await inquirer
            .prompt([
              {
                name: 'executable',
                message: executableFieldDescription
              }
            ])
            .then(({ executable }) => executable)
        }
        let templates = [path.join(templatePath, '**', '*')]
        const scaffold = new SimpleScaffold({
          name,
          output: process.cwd(),
          templates,
          createSubFolder: false,
          locals: {
            year: new Date().getFullYear(),
            author_name,
            author_email,
            project_name: name,
            project_exe: executable || name + '-bin',
            project_summary
          }
        })

        try {
          scaffold.run()
          console.log(chalk.green('Success!'))
        } catch (error) {
          console.log()
          console.log(chalk.red('Scaffolding failed'))
          console.log()
          console.log({ error })
        }
      }
    )

  program
    .command('ls')
    .alias('list')
    .action(() => {
      console.log('Available templates:')

      readdirSync(path.join(__dirname, '..', 'templates'), {
        withFileTypes: true
      })
        .filter((dirent) => dirent.isDirectory())
        .map(({ name }) => name)
        .map((directory) => {
          return chalk.green(`taklgen new ${directory}`)
        })
        .forEach((dir) => console.log(dir))
    })

  await program.parseAsync()
})()
